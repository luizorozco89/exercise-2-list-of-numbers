import React, { useState } from 'react';
import './myComponent.css';
import NumberItem from './NumberItem';

const MyComponent = () => {
  const genRange = () => {
    const innerArr = [];
    for(var i = 0; i <= 100; i++) {
      innerArr.push(i);
    }
    return innerArr;
  }

  const paintBackground = (n) => {
    if((n % 3) === 0 && (n % 5) === 0) {
      return 'greenBackground';
    } else if((n % 3) === 0) {
      return 'redBackground';
    } else if((n % 5) === 0) {
      return 'blueBackground';
    } else {
      return '';
    }
  }

  const [range, setRange] = useState(genRange());

  return(
    <div>
      {range.map((number, index) => {
        let classColor = paintBackground(number);
        return <NumberItem key={index} number={number} classColor={classColor} />
      })}
    </div>
  );
}

export default MyComponent;