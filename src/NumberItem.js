import React from 'react';

const NumberItem = (props) => {
  let { number, classColor } = props;

  return(<p className={classColor}>{number}</p>);
}

export default NumberItem;

